---
title: How can I get involved?
pubdate: 2018-07-18
image: Sacred_Chao_2.jpg
caption: Wikipedia
captionlabel: Source:
captionlink: https://en.wikipedia.org/wiki/File:Sacred_Chao_2.jpg
---

We're so glad you asked! Come on over to the [Svelte](https://github.com/sveltejs/svelte) and [Sapper](https://github.com/sveltejs/sapper) repos, and join us in the [Discord chatroom](https://discord.gg/yy75DKs). Everyone is welcome, especially you!